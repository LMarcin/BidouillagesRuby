require 'state_machine'
require 'highline/import'

class PopVehicle
  state_machine :initial => :parked do

    event :ignite do
      transition  [:parked] => :idling
    end

    event :park do
      transition  [:idling] => :parked
    end

    event :gear_up do
      transition  [:parked] => :first_gear,
                  [:first_gear] => :second_gear,
                  [:second_gear] => :third_gear,
                  [:third_gear] => :crashed
    end

    event :gear_down do
      transition  [:first_gear] => :idling,
                  [:second_gear] => :first_gear,
                  [:third_gear] => :second_gear
    end

    event :crash do
      transition  [:first_gear, :second_gear, :third_gear] => :crashed
    end

    event :repair do
      transition  [:crashed] => :parked
    end

  end
end

vehicle = PopVehicle.new

while true do
  puts "Le véhicule est #{vehicle.state}"
  puts "et peut recevoir : #{vehicle.state_events}"
  input = ask "action ?: "
  vehicle.send(input)
end
